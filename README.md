# Install

1. Go to the setup page and choose **Game Systems**.
2. Click the **Install System** button, and paste in this manifest link:
    * Foundry 0.5.x: [https://gitlab.com/asacolips-projects/foundry-mods/dungeonworld/raw/master/system.json](https://gitlab.com/asacolips-projects/foundry-mods/dungeonworld/raw/master/system.json)
3. Create a Game World using the Dungeon World system.

Compatible with FoundryVTT 0.3.x

# Description

Build campaigns in the Dungeon World RPG using Foundry VTT!

## Character Sheet
![character sheet](https://mattsmithin.nyc3.digitaloceanspaces.com/assets/dw-0.3.0.png)

## Character Builder
![character builder](https://mattsmithin.nyc3.digitaloceanspaces.com/assets/dw-0.3.0-character-builder.png)

## Level Up
![level up](https://mattsmithin.nyc3.digitaloceanspaces.com/assets/dw-0.3.0-level-up.png)

## Combat Tracker
![combat tracker](https://gitlab.com/asacolips-projects/foundry-mods/dungeonworld/uploads/e3ff32b9c9e94c0dd57aeffa7e679e28/image.png)

# Licensing

All HTML, CSS, and JS is licensed under the [MIT license](https://gitlab.com/asacolips-projects/foundry-mods/dungeonworld/-/raw/master/LICENSE.txt).

Compendium content is licensed under the Creative Commons Attribution 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

In addition, the compendium content uses the OPEN GAME LICENSE Version 1.0a. See the [LICENSE-COMPENDIUM.txt](https://gitlab.com/asacolips-projects/foundry-mods/dungeonworld/-/raw/master/LICENSE-COMPENDIUM.txt) file for additional details.

